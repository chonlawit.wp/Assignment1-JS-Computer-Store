const laptopsElement = document.getElementById("laptops")
const featuresElement = document.getElementById("features")
const imageElement = document.getElementById("image")
const titleElement = document.getElementById("title")
const descriptionElement = document.getElementById("description")
const priceElement = document.getElementById("price")
const balanceElement = document.getElementById('balance')
const getLoanButton = document.getElementById('get-loan')
const loanElement = document.getElementById('loan')
const loanList = document.getElementById('loan-list')
const bankButton = document.getElementById('bank-button')
const workButton = document.getElementById('work-button')
const payElement = document.getElementById('pay')
const repayButton = document.getElementById('repay-button')
const buyButton = document.getElementById('buy-button')


let laptops = []
let bankBalance = 0
let loan = 0
let salary = 0
let laptopPrice = 0

updateBalance(200)

function updateBalance(inputAmount) {
    bankBalance += Number(inputAmount)
    balanceElement.innerHTML = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(bankBalance)
}

function updateLoan(inputLoan) {
    loan += Number(inputLoan)
    loanElement.innerHTML = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(loan)
}

function updateSalary(inputSalary) {
    salary += Number(inputSalary)
    payElement.innerHTML = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(salary)
}

function downPayment() {
    let loan = Number(loanElement.innerHTML)
    if (loan <= 0) {
        loanList.style.visibility = "hidden"
        repayButton.style.visibility = "hidden"
    }
}

getLoanButton.addEventListener('click', function () {
    const inputLoan = Number(prompt("Apply for loan"))

    if (isNaN(inputLoan) || inputLoan <= 0) {
        alert("Invalid amount")
    } else if (inputLoan > (bankBalance * 2)) {
        alert("You cannot get a loan more than double of your bank balance")
    } else if (loan > 0) {
        alert("You cannot get more than one bank loan before repaying the last loan")
    } else {
        loanList.setAttribute("style", "visibility: visible")
        repayButton.removeAttribute("hidden")
        updateLoan(inputLoan)
        updateBalance(inputLoan)
    }
})

workButton.addEventListener('click', function () {
    updateSalary(100)
})


bankButton.addEventListener('click', function () {
    let tenPercentOfSalary = salary * 0.1
    let restOfSalary = salary - tenPercentOfSalary
    if (loan > 0) {
        updateLoan(-tenPercentOfSalary)
        updateBalance(restOfSalary)
        updateSalary(-salary)
    }
    else {
        updateBalance(salary)
        updateSalary(-salary)
    }

    if (loan <= 0) {
        loanList.style.visibility = "hidden"
    }
})

repayButton.addEventListener('click', function () {
    let rest = salary - loan

    if (salary < loan) {
        updateLoan(-salary)
        updateSalary(-salary)
    } else {
        updateLoan(-loan)
        updateBalance(rest)
        updateSalary(-salary)
    }

    if (loan <= 0) {
        loanList.style.visibility = "hidden"
    }
})

buyButton.addEventListener('click', function () {
    let laptopTitle = titleElement.innerHTML

    if (laptopPrice > bankBalance) {
        alert("You don't have suffient balance to buy this laptop")
    } else {
        updateBalance(-laptopPrice)
        alert(`You have purchased a ${laptopTitle}`)
    }
})


fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x));
}

const addLaptopToMenu = (laptops) => {
    const laptopElement = document.createElement("option")
    laptopElement.value = laptops.id;
    laptopElement.appendChild(document.createTextNode(laptops.title))
    laptopsElement.appendChild(laptopElement)

}


laptopsElement.addEventListener("change", function (event) {
    document.getElementById("productInfo").removeAttribute("hidden")
    const laptopId = event.target.value;
    const laptop = findLaptopById(laptops, laptopId)
    outputLaptop(laptop)

})


const findLaptopById = (laptops, laptopId) => {
    return laptops.find(function (laptop) {
        return laptop.id === Number(laptopId)
    });
};


function outputLaptop(laptop) {
    imageElement.setAttribute("src", `https://noroff-komputer-store-api.herokuapp.com/${laptop.image}`)
    titleElement.innerHTML = laptop.title


    laptopPrice = laptop.price
    priceElement.innerHTML = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(laptop.price)

    descriptionElement.innerHTML = laptop.description


    let str = '<h3>Features:</h3><ul style="list-style-type:none">'
    laptop.specs.forEach(function (spec) {
        str += "<li>" + spec + "</li>";
    });
    str += "</ul>"
    featuresElement.innerHTML = str;

}






